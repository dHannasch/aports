# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=kubeseal
pkgver=0.20.1
pkgrel=0
pkgdesc="One-way encrypted Secrets tool for Kubernetes"
url="https://sealed-secrets.netlify.app/"
arch="all"
license="Apache-2.0"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/bitnami-labs/sealed-secrets/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/sealed-secrets-$pkgver"

# secfixes:
#   0.19.0-r0:
#     - CVE-2022-32149

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v \
		-o bin/$pkgname \
		-ldflags "-X main.VERSION=$pkgver" \
		./cmd/kubeseal
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/$pkgname -t "$pkgdir"/usr/bin
}

sha512sums="
9b8a4776270155fb4cc78ed8bb6468d290b0e70bf4a85168cf474d5fa8cd488702114b655c3a71df07930c7b776312933c7cd55daa8429390e5160b87218eead  kubeseal-0.20.1.tar.gz
"
