# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=font-iosevka
pkgver=21.1.1
pkgrel=0
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-aile
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base=$pkgver-r$pkgrel
		$pkgname-aile=$pkgver-r$pkgrel
		$pkgname-slab=$pkgver-r$pkgrel
		$pkgname-curly=$pkgver-r$pkgrel
		$pkgname-curly-slab=$pkgver-r$pkgrel
		"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/iosevka.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/iosevka-aile.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/iosevka-curly-slab.ttc
}

sha512sums="
44a1ccc0d15bcde9625c38c86140d0eb843dd1b4a799c939d865d118cc63baa215ed275f7a22bec06c8a0c1a4fc10bc972f6da97f91613426894958183efda34  super-ttc-iosevka-21.1.1.zip
a1bbbcdb90874a24e1ed4f30a86546ff4d135ef67e02f3c001990a6a781492ed0b99bfcee22c313b5a1513d82dd34e5cf272822182f14c73c7c824b1f575c787  super-ttc-iosevka-aile-21.1.1.zip
02ccf26a2ba0b4f354ca0dbd539843bc16d47bf1b602786290c61e1efaae68808245f7187a1acc81788bf04df736e85d49992f2a4838289ce84f87dc86ef9357  super-ttc-iosevka-slab-21.1.1.zip
48a3bdb9fdaa09695e01593e1f07c35ef6dcf3902aae2d940189b90246411c7f2cd814aa8293ded4996fd06809e6c900f01faddcc889a8c12eb4711e0001d4ad  super-ttc-iosevka-curly-21.1.1.zip
8e34d67900362bfdc979eeaf648d09cd8f4be031e5873d8aa3a86a37c3aadcbcb5d959bfc870c6147d676489b7f7d469d56ece3b1c0dd9700947b516eb7106a0  super-ttc-iosevka-curly-slab-21.1.1.zip
"
