# Maintainer: Eivind Uggedal <eu@eju.no>
pkgname=py3-redis
_pkgname=redis
pkgver=4.5.2
pkgrel=0
pkgdesc="Python3 client for Redis key-value store"
url="https://github.com/andymccurdy/redis-py"
arch="noarch"
license="MIT"
depends="python3 py3-async-timeout py3-hiredis"
makedepends="py3-setuptools py3-gpep517 py3-wheel"
checkdepends="py3-pytest py3-mock redis"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir"/$_pkgname-$pkgver
options="!check" # tests fail due to old version + disabled proprietary redis features

replaces="py-redis" # Backwards compatibility
provides="py-redis=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	redis-server --dir "$builddir" &
	trap "kill $!" EXIT
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
6144d1a01750c5a5f48d58caff9c20239846f8f88fa29c665897a14075777490d0ec0eab7ea20537bd391a896b50ffb7366bf48f2c90ee66bc6d0f5d16e3ed70  redis-4.5.2.tar.gz
"
