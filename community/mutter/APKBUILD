# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=mutter
pkgver=44.0
pkgrel=0
pkgdesc="clutter-based window manager and compositor"
url="https://wiki.gnome.org/Projects/Mutter/"
arch="all"
license="GPL-2.0-or-later"
depends="
	gnome-settings-daemon
	gsettings-desktop-schemas
	mutter-schemas
	xkeyboard-config
	"
makedepends="
	at-spi2-core-dev
	cairo-dev
	dbus-dev
	elogind-dev
	eudev-dev
	fribidi-dev
	gdk-pixbuf-dev
	glib-dev
	gnome-desktop-dev
	gnome-settings-daemon-dev
	gobject-introspection-dev
	graphene-dev
	gtk+3.0-dev
	json-glib-dev
	libcanberra-dev
	libdrm-dev
	libgudev-dev
	libice-dev
	libinput-dev
	libsm-dev
	libwacom-dev
	libx11-dev
	libxau-dev
	libxcb-dev
	libxcomposite-dev
	libxcursor-dev
	libxdamage-dev
	libxext-dev
	libxfixes-dev
	libxi-dev
	libxinerama-dev
	libxkbcommon-dev
	libxkbfile-dev
	libxrandr-dev
	libxrender-dev
	libxtst-dev
	mesa-dev
	meson
	pango-dev
	pipewire-dev
	startup-notification-dev
	wayland-dev
	wayland-protocols
	xkeyboard-config-dev
	xwayland-dev
	"
options="!check" # Can't be run with release builds
subpackages="$pkgname-dbg $pkgname-dev $pkgname-doc $pkgname-lang $pkgname-schemas::noarch"
source="https://download.gnome.org/sources/mutter/${pkgver%.*}/mutter-$pkgver.tar.xz
	fixudev-req.patch
	fix-crashes.patch
	pcversion.patch
	"

build() {
	abuild-meson \
		-Db_lto=true \
		-Degl_device=true \
		-Dudev=true \
		-Dnative_backend=true \
		-Dintrospection=true \
		-Dremote_desktop=true \
		-Dprofiler=false \
		-Dtests=false \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

schemas() {
	pkgdesc="Mutter schemas related files"
	depends=""

	amove usr/share/glib-2.0/schemas
	amove usr/share/GConf/gsettings
	amove usr/share/gnome-control-center/keybindings
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
c7d35e797d080f90cc43b2904bb04d2328fecd13cd7d1ab12b1db735d331b2df9501668c2e0f7a10ea3ad9821048b09cbeb7149fcda3d43f37e5a1834bb04e2b  mutter-44.0.tar.xz
3e5dd59f2f2fd80edde13f64d70d52a023dec303fd8dbfa3ded0d77faf7643179f2ad74d4acd3450decb67deaf6ac85a7af5146fa96f33917b4363f884413af9  fixudev-req.patch
9c81f1e39385b8d3d73b0b680a4a6c7dae6a581e99f026f472864125779268962a114c2ee80666e1810a87c3659beaa334e2e08c25f7b82b329a2b716e4bef35  fix-crashes.patch
bb46a4692aaff0c9e6092d1ba80d19b336035e983f0f20a437a586757bc2530860f50fbd46b09f73e88f0dd80ea7188d1df467b8f762918717785e618b6e9c4b  pcversion.patch
"
