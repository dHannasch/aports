# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=gucharmap
pkgver=15.0.3
pkgrel=0
pkgdesc="GNOME Unicode Charmap"
url="https://wiki.gnome.org/Apps/Gucharmap"
arch="all"
license="GPL-3.0-or-later GFDL-1.3-only"
makedepends="
	desktop-file-utils
	glib-dev
	gobject-introspection-dev
	gtk+3.0-dev
	itstool
	meson
	pcre2-dev
	perl
	unicode-character-database>=${pkgver/.*/}.0.0
	unzip
	vala
	"
subpackages="$pkgname-dev $pkgname-lang"
source="https://gitlab.gnome.org/GNOME/gucharmap/-/archive/$pkgver/gucharmap-$pkgver.tar.gz"

build() {
	# Disable docs due to https://github.com/mesonbuild/meson/issues/5843
	abuild-meson \
		-Ducd_path=/usr/share/unicode \
		-Ddocs=false \
		output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
72590f6f9dbc57d2117e7b22475be3a15f73f60842615975c22c3bd821c6d155bcc5760e9b86a17b601a1438a630538c28131f2543f6fc2733d2b403a4829474  gucharmap-15.0.3.tar.gz
"
